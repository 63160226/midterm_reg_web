import 'package:flutter/material.dart';
import 'package:web_reg/Drawers.dart';



class ScheduleScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: DrawerWidget(),
        appBar: AppBar(
          backgroundColor: Color.lerp(Colors.amber, Colors.grey, 0.5),
          actions: <Widget>[
            Image.network(
              "https://scontent.futp1-2.fna.fbcdn.net/v/t1.15752-9/272205348_941955753137920_3960106178236359557_n.png?_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeFCdkS24v5fiyezcoR6MIio2rbVIn_iLI7attUif-IsjhBJkMKxltS3Wzuj13woRfLzSbYcR_2HFD1FsW7rLyom&_nc_ohc=KF-BVvq-tVsAX-yzKkv&_nc_ht=scontent.futp1-2.fna&oh=03_AdTV4o0-LLqe02gQEub3yNkHx_78svXk4yDqbP61TahirQ&oe=64018E26",
              height: 100.0,
              width: 100.0,
            ),
          ],
          title: Text('ระบบลงทะเบียนมหาวิทยาลัยบูรพา'),
        ),
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.grey, Colors.amber],
              ),
            ),
            child: Table(
              children: [
                TableRow(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 2),
                      color: Colors.white54),
                  children: <Widget>[
                    ListTile(
                      title: Text('Monday'),
                    ),
                    ListTile(
                      title: Text('10:00 - 12:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624559-59 Software Testing\nตึกเรียน IF \nห้องเรียน 4M210'),
                    ),
                    ListTile(
                      title: Text('13:00 - 15:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624459-59 Object-Oriented Analysis and Design\nตึกเรียน IF \nห้องเรียน 3M210'),
                    ),
                    ListTile(
                      title: Text('17:00 - 19:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624359-59 Web Programming\nตึกเรียน IF \nห้องเรียน 3M210'),
                    ),
                  ],
                ),
                TableRow(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 2),
                      color: Colors.white54),
                  children: <Widget>[
                    ListTile(
                      title: Text('Tuesday'),
                    ),
                    ListTile(
                      title: Text('10:00 - 12:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n8634259-59 Multimedia Programming for Multiplatforms\nตึกเรียน IF \nห้องเรียน 4C02'),
                    ),
                    ListTile(
                      title: Text('13:00 - 15:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624559-59 Software Testing\nตึกเรียน IF \nห้องเรียน 3C03'),
                    ),
                    ListTile(
                      title: Text('17:00 - 19:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624459-59 Object-Oriented Analysis and Design\nตึกเรียน IF \nห้องเรียน 3C01'),
                    ),
                  ],
                ),
                TableRow(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 2),
                      color: Colors.white54),
                  children: <Widget>[
                    ListTile(
                      title: Text('Wednesday'),
                    ),
                    ListTile(
                      title: Text('10:00 - 12:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n8634459-59 Mobile Application Development I\nตึกเรียน IF \nห้องเรียน 4C02'),
                    ),
                    ListTile(
                      title: Text('13:00 - 15:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624559-59 Software Testing\nตึกเรียน IF \nห้องเรียน 3C03'),
                    ),
                    ListTile(
                      title: Text('17:00 - 19:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88624459-59 Object-Oriented Analysis and Design\nตึกเรียน IF \nห้องเรียน 3C01'),
                    ),
                  ],
                ),
                TableRow(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 2),
                      color: Colors.white54),
                  children: <Widget>[
                    ListTile(
                      title: Text('Thursday'),
                    ),
                    ListTile(
                      title: Text(''),
                    ),
                    ListTile(
                      title: Text(''),
                    ),
                    ListTile(
                      title: Text(''),
                    ),
                  ],
                ),
                TableRow(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 2),
                    color: Colors.white54,
                  ),
                  children: <Widget>[
                    ListTile(
                      title: Text('Friday'),
                    ),
                    ListTile(
                      title: Text('9:00 - 12:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n88646259-59 Introduction to Natual Language\nตึกเรียน IF \nห้องเรียน 5T05'),
                    ),
                    ListTile(
                      title: Text('13:00 - 15:00'),
                      subtitle: Text(
                          'กลุ่มเรียน 2\n8634459-59 Mobile Application Development I\nตึกเรียน IF \nห้องเรียน 4C02'),
                    ),
                    ListTile(
                      title: Text(''),
                      subtitle: Text(''),
                    ),
                  ],
                ),
              ],
            ),
            //Monday schedule
            //Tuesday schedule
            //and so on for the rest of the week
          ),
        ),
      ),
    );
  }
}
