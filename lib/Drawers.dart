import 'package:flutter/material.dart';
import 'package:web_reg/Profile.dart';
import 'package:web_reg/data/course.dart';
import 'package:web_reg/timeclass.dart';

class DrawerWidget extends StatelessWidget {
  late final Course course;
  @override
  Widget build(BuildContext context) {

    return Drawer(
      backgroundColor: Color.lerp(Colors.amber, Colors.grey, 0.5),
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              image: DecorationImage(
                image: NetworkImage(
                    'https://scontent.futp1-1.fna.fbcdn.net/v/t1.15752-9/328099818_915688419588972_1867812444760609798_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeHGKDg66XKWDbH0h-Sv5TPcu_224e82j5S7_bbh7zaPlAksfBgwX6_ZP2tUZfHPAuGUGzPQ9eUAlOzqUbwYkDJc&_nc_ohc=ejz8-PvcEYoAX_lcLLF&_nc_ht=scontent.futp1-1.fna&oh=03_AdQ5hSQNa2QiTIJzfD9g2L3vJfRsSSEpZP_EncLqeM7HaA&oe=63FF1265'),
                fit: BoxFit.fitHeight,
              ),
              gradient: LinearGradient(
                  end: Alignment.topCenter,
                  colors: [Colors.grey, Colors.amber]),
            ),
            child: null,
          ),
          Text(
            "Email:  63160226@go.buu.ac.th\n\n"
                "เบอร์โทรศัพท์: 0983876861\n",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text('Profile'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Profile()),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.book_online),
            title: const Text('Class'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>  ScheduleScreen()),
              );
            },
          ),
        ],
      ),
    );
  }
  Widget _scrollableView() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Column(
          children: <Widget>[
            Container(
              height: 200,
              color: course.color,
              child: const Center(
                child: Icon(Icons.image),
              ),
            ),
            const SizedBox(height: 61),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                children: [
                  Container(
                    color: Colors.grey,
                    height: 0.5,
                  ),
                  const SizedBox(height: 20),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      course.title,
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  const SizedBox(height: 5),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      course.description,
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  ...course.lessons.map((lesson) {
                    return Row(
                      children: [
                        const Text("• "),
                        Expanded(
                          child: Text(lesson),
                        ),
                      ],
                    );
                  }),
                ],
              ),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
