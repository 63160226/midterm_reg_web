import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:web_reg/Drawers.dart';


void main() {
  runApp(DevicePreview(
    enabled: true,
    builder: (context) => const Profile(),
  ));
}

class Profile extends StatelessWidget {
  const Profile({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      useInheritedMediaQuery: true,
      builder: DevicePreview.appBuilder,
      locale: DevicePreview.locale(context),
      home: Scaffold(
        drawer: DrawerWidget(),
        appBar: AppBar(
          backgroundColor: Color.lerp(Colors.amber, Colors.grey, 0.5),
          actions: <Widget>[
            Image.network(
              "https://scontent.futp1-2.fna.fbcdn.net/v/t1.15752-9/272205348_941955753137920_3960106178236359557_n.png?_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeFCdkS24v5fiyezcoR6MIio2rbVIn_iLI7attUif-IsjhBJkMKxltS3Wzuj13woRfLzSbYcR_2HFD1FsW7rLyom&_nc_ohc=KF-BVvq-tVsAX-yzKkv&_nc_ht=scontent.futp1-2.fna&oh=03_AdTV4o0-LLqe02gQEub3yNkHx_78svXk4yDqbP61TahirQ&oe=64018E26",
              height: 100.0,
              width: 100.0,
            ),
          ],
          title: Text('ระบบลงทะเบียนมหาวิทยาลัยบูรพา'),
        ),

        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.grey, Colors.amber],
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 640,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        end: Alignment.bottomCenter,
                        colors: [Colors.grey, Colors.amber],
                      ),
                    ),
                    child: Container(
                      width: 200,
                      height: 200,
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 50, bottom: 300),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.yellow, width: 2),
                        image: DecorationImage(
                          image: NetworkImage(
                              'https://scontent.futp1-1.fna.fbcdn.net/v/t1.15752-9/328099818_915688419588972_1867812444760609798_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeHGKDg66XKWDbH0h-Sv5TPcu_224e82j5S7_bbh7zaPlAksfBgwX6_ZP2tUZfHPAuGUGzPQ9eUAlOzqUbwYkDJc&_nc_ohc=ejz8-PvcEYoAX_lcLLF&_nc_ht=scontent.futp1-1.fna&oh=03_AdQ5hSQNa2QiTIJzfD9g2L3vJfRsSSEpZP_EncLqeM7HaA&oe=63FF1265'),
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 2),
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white54,
                    ),
                    margin: EdgeInsets.only(
                        left: 50, right: 50, top: 0, bottom: 200),
                    child: Row(
                      children: [
                        Text("รหัสประจำตัว:  63160226\n"
                            "เลขที่บัตรประชาชน: 1279900184811\n"
                            "ชื่อ:  นายสาธิต วาปีเตา\n"
                            "ชื่ออังกฤษ:  MR. SATIT WAPEETAO\n"
                            "คณะ: คณะวิทยาการสารสนเทศ\n"
                            "วิทยาเขต:  บางแสน\n"
                            "หลักสูตร:  2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n"
                            "วิชาโท:  -\n"),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 2),
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white54,
                    ),
                    margin: EdgeInsets.only(
                        left: 50, right: 50, top: 0, bottom: 200),
                    child: Row(
                      children: [
                        Text("ระดับการศึกษา: ปริญญาตรี\n"
                            "ชื่อปริญญา:  วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n"
                            "ปีการศึกษาที่เข้า: 2563 / 1\n"
                            "วันที่ 21/5/2563\n"
                            'สถานภาพ:\n'
                            "วิธีรับเข้า: รับตรงทั่วประเทศ\n"
                            "วุฒิก่อนเข้ารับการศึกษา: ม.6/2.63\n"
                            "จบการศึกษาจาก: ดาราสมุทร\n"
                            "อ.ที่ปรึกษา: ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม\n"),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
